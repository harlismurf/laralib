<div class="form-group">
  <label class="control-label col-md-3" for="title">Title <span class="required">*</span></label>
  <div class="col-md-7">
    <input type="text" name="title" id="title" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div>            
<div class="form-group">
  <label class="control-label col-md-3" for="isbn">ISBN</label>
  <div class="col-md-7">
    <input type="text" name="isbn" id="isbn" required="required" class="form-control" data-inputmask="'mask': '999-999-99-9999-9'">
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3" for="edition">Edition</label>
  <div class="col-md-2">
    <input type="text" name="edition" id="edition" required="required" class="form-control col-md-2 col-xs-12">
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3" for="volume">Volume</label>
  <div class="col-md-2">
    <input type="text" name="volume" id="volume" required="required" class="form-control col-md-2 col-xs-12">
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3" for="issue">Issue (optional)</label>
  <div class="col-md-7">
    <textarea name="issue" id="issue" required="required" class="form-control" data-parsley-maxlength="100"></textarea>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12">Publisher</label>
  <div class="col-md-7">
    <select class="form-control" name="publisher_id" id="publisher_id" required="required">
      <option disabled selected>--Pilih Publisher--</option>
      @foreach ($publishers as $key => $publisher)
          <option value="{{ $publisher->id }}" @if (old('publisher_id') == $publisher->id) selected='selected' @endif >
            {{ $publisher->name }}
      @endforeach
    </select>
  </div>
</div>
<div class="form-group">
  <label class="control-label col-md-3" for="cover">Cover</label>
  <div class="col-md-7">
    <input type="file" name="cover" id="cover" required="required" class="form-control col-md-7 col-xs-12">
  </div>
</div> 