@extends('layouts.admin.app')

@section('quick_profile')
	<div class="profile_pic">
	    <img src="{{ asset('assets/home_admin/images/lib.png') }}" alt="..." class="img-circle profile_img">
	</div>
	<div class="profile_info">
		<span>Welcome,</span>
		{{-- <h2>{{ Auth::user()->name }}</h2> --}}
	</div>
@endsection

@section('sidebar')
	  <div class="menu_section">
	    <h3>General</h3>
	    <ul class="nav side-menu">
	      <li><a href="/librarian/home"><i class="fa fa-home"></i>Dashboard</a></li>
	      <li><a><i class="fa fa-book"></i> Manage Books <span class="fa fa-chevron-down"></span></a>
	        <ul class="nav child_menu">
	          <li><a href="/librarian/books">Books</a></li>
	          <li><a href="/librarian/ebooks">E-Books</a></li>
	      		<li><a href="#"> -- Categories </a></li>
	        </ul>
	      </li>
	      <li><a><i class="fa fa-users"></i> Manage Members <span class="fa fa-chevron-down"></span></a>
	        <ul class="nav child_menu">
	          <li><a href="/librarian/member">User</a></li>
	          <li><a href="form_advanced.html">E-Books</a></li>
	      		<li><a href="#"> -- Categories </a></li>
	        </ul>
	      </li>
	      <li><a href="#"><i class="fa fa-random"></i>Donation</a></li>
	      <li><a href="#"><i class="fa fa-area-chart"></i>Borrowed Books</a></li>
	      <li><a href="#"><i class="fa fa-area-chart"></i>Daily Read Books</a></li>
	      <li><a href="#"><i class="fa fa-home"></i>Requested Books</a></li>
	      <li><a href="#"><i class="fa fa-home"></i>Report</a></li>
	    </ul>
	  </div>
@endsection

@section('content')
<head>

    <!-- Datatables -->
    <link href="{{ asset('assets/home_admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/home_admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/home_admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/home_admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/home_admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
  </head>
  
			<div class="row top_tiles">
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats" style="background:#00c0ef;">
						<a href="/librarian/books">
							<div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
							<div class="count" style="color:white;">1515</div>
							<h3 style="color:white;">Books</h3>
						</a>
					</div>
				</div>
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats" style="background:#00a65a;">
						<a href="#">
							<div class="icon"><i class="fa fa-comments-o"></i></div>
							<div class="count" style="color:white;">0</div>
							<h3 style="color:white;">Borrowed Books</h3>
						</a>
					</div>
				</div>
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats" style="background:#dd4b39;">
						<a href="#">
							<div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
							<div class="count" style="color:white;">Return</div>
							<h3 style="color:white;">Books</h3>
						</a>
					</div>
				</div>
				<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="tile-stats" style="background:#dd4b39;">
						<a href="#">
							<div class="icon"><i class="fa fa-check-square-o"></i></div>
							<div class="count" style="color:white;">Issue</div>
							<h3 style="color:white;">Books</h3>
						</a>
					</div>
				</div>
			</div>
@endsection

<!-- Datatables -->
    <script src="{{ asset('assets/home_admin/vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/jszip/dist/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/pdfmake/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/home_admin/vendors/pdfmake/build/vfs_fonts.js') }}"></script>