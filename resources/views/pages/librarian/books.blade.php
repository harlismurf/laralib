@extends('pages.librarian.home')

@section('content')
	    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Books <small>data</small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <table id="datatable-buttons" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>title</th>
                  <th>ISBN</th>
                  <th>Edition</th>
                  <th>Volume</th>
                  <th>Publisher</th>
                  <th>
                    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">
                      <i class="glyphicon glyphicon-plus"></i> New Data
                    </button>
                  </th>
                </tr>
              </thead>
              <tbody>
            {{ csrf_field() }}
			<?php $no=1; ?>

			@foreach ($books as $key => $value)
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $value->title }}</td>
                  <td>{{ $value->isbn }}</td>
                  <td>{{ $value->edition }}</td>
                  <td>{{ $value->volume }}</td>
                  <td>{{ $value->name }}</td>
                  <td>
                    <a href="#" class="edit-modal btn btn-warning btn-sm" data-mytitle="{{ $value->title }}" data-myisbn="{{ $value->isbn }}" data-myedition="{{ $value->edition }}" data-myvolume="{{ $value->volume }}" data-myissue="{{ $value->issue }}" data-mycover="{{ $value->cover }}" data-bookid="{{$value->id}}" data-toggle="modal" data-target="#edit">
                        <i class="fa fa-pencil"></i>
                    </a>
                    &nbsp
                    <a href="#" class="delete-modal btn btn-danger btn-sm" data-toggle="modal" data-target="#delete">
                        <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
			@endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>


  <!-- Modal New Books-->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">New Books</h4>
        </div>
        <form action="{{route('books.store')}}" method="POST" class="form-horizontal form-label-left">
          {{csrf_field()}}
          <div class="modal-body">
            
            @include('pages.librarian.form.addBooks')

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="reset" class="btn btn-danger">Reset</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>

  <!-- Modal Edit Books-->
  <div class="modal fade" id="edit" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Books</h4>
        </div>
        <form action="{{route('books.update', 1)}}" method="post" class="form-horizontal form-label-left">
          {{method_field('patch')}}
          {{csrf_field()}}
          <div class="modal-body">

            <input type="hidden" name="book_id" id="book_id" value="" >
            @include('pages.librarian.form.editBooks')

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Changes</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>


  <!-- Modal Delete Books-->
  <div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Books</h4>
        </div>
        <form action="{{route('books.destroy', 'asd')}}" method="post" class="form-horizontal form-label-left">
          {{method_field('delete')}}
          {{csrf_field()}}
          <div class="modal-body">
            <p><center>
              <h3>Are you sure want to delete this ?</h3>
            </p></center>
            <input type="hidden" name="book_id" id="book_id" value="">

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
            <button type="submit" class="btn btn-warning">Yes, Delete</button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
@endsection
