@extends('layouts.member.app')

@section('content')
<div class="span9">
    <div class="content">
        <div class="btn-controls">
            <div class="btn-box-row row-fluid">
                <a href="#" class="btn-box big span4" style="background-color: #ffc800;">
                    <i class=" icon-random"></i><b>65%</b>
                    <p class="text-muted">Reading Challange</p>
                </a>
                <a href="#" class="btn-box big span4">
                    <i class="icon-user"></i><b>15</b>
                    <p class="text-muted">New Friends</p>
                </a>
                <a href="#" class="btn-box big span4"><i class="icon-money">
                    </i><b>15,152</b>
                    <p class="text-muted">Points</p>
                </a>
            </div>
            <div class="btn-box-row row-fluid">
                <div class="span8">
                    <div class="row-fluid">
                        <div class="span12">
                            <a href="#" class="btn-box small span4"><i class="icon-envelope"></i><b>Messages</b>
                            </a><a href="#" class="btn-box small span4"><i class="icon-group"></i><b>Clients</b>
                            </a><a href="#" class="btn-box small span4"><i class="icon-exchange"></i><b>Expenses</b>
                            </a>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <a href="#" class="btn-box small span4">
                                <i class="icon-save"></i><b>Total Sales</b>
                            </a>
                            <a href="#" class="btn-box small span4">
                                <i class="icon-bullhorn"></i><b>Social Feed</b>
                            </a>
                            <a href="#" class="btn-box small span4">
                                <i class="icon-sort-down"></i><b>Bounce Rate</b> 
                            </a>
                        </div>
                    </div>
                </div>
                <ul class="widget widget-usage unstyled span4">
                    <div class="module-head">
                        <h3>
                            <div align="center">
                                <font size="3"> Best Seller Books </font>
                            </div>
                        </h3>
                    </div>
                    <li>
                        <p>
                            <strong>Demokrasi Ala Tukang Copet</strong> <span class="pull-right small muted">78%</span>
                        </p>
                        <div class="progress tight">
                            <div class="bar" style="width: 78%;">
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>
                            <strong>Dalang Galau Ngetweet</strong> <span class="pull-right small muted">44%</span>
                        </p>
                        <div class="progress tight">
                            <div class="bar bar-warning" style="width: 44%;">
                            </div>
                        </div>
                    </li>
                    <li>
                        <p>
                            <strong>Ngawur Karna Benar</strong> <span class="pull-right small muted">67%</span>
                        </p>
                        <div class="progress tight">
                            <div class="bar bar-danger" style="width: 67%;">
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!--/#btn-controls-->
    </div>
    <!--/.content-->
</div>
@endsection