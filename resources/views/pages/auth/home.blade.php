@extends('layouts.member.app')

@section('content')
<div class="module">
    <div class="module-head">
        <h3>
            <div align="center">
                <font size="3">
                    Welcome to Lara<font color="#1d809f">Lib</font> 
                    {{ Auth::user()->name.' '.Auth::user()->lastname }}
                </font>
            </div>
        </h3>
    </div>
    <div class="module-body">
        <div class="stream-composer media">
            <div class="clearfix" align="center">
                <font size="4" face="Times New Roman">
                      You can meet and read your favorite book from every years ago and from your favorite writer anytime and everytime you want.  
                </font>
            </div>
        </div>
    </div>
    <br>
</div>

<div class="module">
    <div class="module-head">
        <h3>
            <div align="center">
                <font size="3"> Books you've been Read </font>
            </div>
        </h3>
    </div>
    <div class="media stream">
        <a href="#" class="media-avatar medium pull-left">
            <img src="{{asset ('assets/home_member/images/user.png') }}">
        </a>
        <div class="media-body">
            <div class="stream-headline">
                <h5 class="stream-author">
                    {{ Auth::user()->name.' '.Auth::user()->lastname }}
                </h5>
                <div class="stream-text">
                      Si Juki adalah suatu serial komik yang bergenre komedi dimana digemari oleh kalangan remaja karena komedi nya yang khas dan pewarnaan yang menarik membuat komik ini menjadi best seller setiap diluncurkan versi terbarunya.
                </div>
                <div class="stream-attachment photo">
                    <a href="https://www.gramedia.com/products/si-juki-lika-liku-anak-kos">
                        <img src="https://cdn.gramedia.com/uploads/items/716010920_si_juki_-_lika-liku_anak_kos.jpg" width="200" height="200"/>
                    </a>
                    <div class="rating" align="left" >
                        <span>☆</span><span>☆</span><span>☆</span><span>☆</span><span>☆</span>
                    </div>
                    <div class="media-body">
                        <div class="row-fluid">
                            <textarea class="span12" style="height: 70px; resize: none;" placeholder="Write a Review"></textarea>
                            <div class="clearfix">
                                <a href="#" class="btn btn-primary pull-right">
                                    Submit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.media .stream-->
        </div>
    </div>
</div>

<div class="module">
    <div class="module-head">
        <h3>
            <div align="center">
                <font size="3"> Recomendations from librarian </font>
            </div>
        </h3>
    </div>
    <div class="media stream">
        <a href="#" class="media-avatar medium pull-left">
            <img src="{{asset ('assets/home_member/images/user.png') }}">
        </a>
        <div class="media-body">
            <div class="stream-headline">
                <h5 class="stream-author">
                     Librarian 1
                </h5>
                <div class="stream-text">
                      Intelegensi Embun Pagi adalah salah satu seri buku yang diterbitkan oleh dewi lestari atau yang biasa disapa "Dee Lestari" yang saling berhubungan antara buku awal terhadap buku selanjutnya
                </div>
                <div class="stream-attachment photo">
                    <a href="https://www.gramedia.com/products/supernova-inteligensi-embun-pagi">
                        <img src="https://images.gr-assets.com/books/1454636286l/28937283.jpg" width="200" height="200"/>
                    </a>
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                    <h3>
                        Rating :
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                    </h3>
                </div>
            </div>
        </div><!--/.media .stream-->
    </div>
</div>

<div class="module">
<div class="module-head">
    <h3>
        <div align="center">
            <font size="3">
                Recomendations video from librarian
            </font>
        </div>
    </h3>
    <div class="media stream">
        <a href="#" class="media-avatar medium pull-left">
            <img src="{{asset ('assets/home_member/images/user.png') }}">
        </a>
        <div class="media-body">
            <div class="stream-headline">
                <h5 class="stream-author">
                    Media 1
                </h5>
                <div class="stream-text">
                     Sebuah buku dapat merubah cara pemikiran dan pola pikir seseorang dan video ini disebutkan beberapa buku yang dapat merubah pemikiran anda
                </div>
                <div class="stream-attachment video">
                    <div class="responsive-video">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/ocJ-pabJjoY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div><!--/.stream-headline-->
        </div>
    </div>
</div>
@endsection
