@extends('layouts.admin.app')

@section('quick_profile')
	<div class="profile_pic">
	    <img src="{{ asset('assets/home_admin/images/img.jpg') }}" alt="..." class="img-circle profile_img">
	</div>
	<div class="profile_info">
		<span>Welcome,</span>
		<h2>{{ Auth::user()->name }}</h2>
	</div>
@endsection

@section('sidebar')
	  <div class="menu_section">
	    <h3>General</h3>
	    <ul class="nav side-menu">
	      <li><a href="/admin/home"><i class="fa fa-home"></i>Dashboard</a></li>
	      <li><a><i class="fa fa-edit"></i> Manage Users <span class="fa fa-chevron-down"></span></a>
	        <ul class="nav child_menu">
	          <li><a href="#">Librarian</a></li>
	          <li><a href="#">Member</a></li>
	        </ul>
	      </li>
	    </ul>
	  </div>
@endsection

@section('content')
	<h4> You're Login as Super Admin </h4>
@endsection