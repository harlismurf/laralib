<div class="sidebar">
    <ul class="widget widget-menu unstyled">
        <li class="active"><a href="/home"><i class="menu-icon icon-dashboard"></i>Dashboard
        </a></li>
        <li><a href="#"><i class="menu-icon icon-bullhorn"></i>News Feed </a>
        </li>
        <li><a href="/home/inbox"><i class="menu-icon icon-inbox"></i>Inbox <b class="label green pull-right">
            11</b> </a></li>
        <li><a href="/home/task"><i class="menu-icon icon-tasks"></i>Tasks <b class="label orange pull-right">
            19</b> </a></li>
    </ul>
    <!--/.widget-nav-->

    <ul class="widget widget-menu unstyled">
      <li><a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-cog">
      </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
      </i>Browse </a>
          <ul id="togglePages" class="collapse unstyled">
              <li><a href="other-login.html"><i class="icon-inbox"></i>Books </a></li>
              <li><a href="other-user-profile.html"><i class="icon-inbox"></i>e-Books </a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Other References </a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Categories</a></li>
          </ul>
      </li>
        <li><a href="ui-typography.html"><i class="menu-icon icon-book"></i>Want to Read </a></li>
        <li><a href="form.html"><i class="menu-icon icon-paste"></i>Bookmarks </a></li>
        <li><a class="collapsed" data-toggle="collapse" href="#categories"><i class="menu-icon icon-cog">
      </i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right">
      </i>Categories </a>
          <ul id="categories" class="collapse unstyled">
              <li><a href="other-login.html"><i class="icon-inbox"></i>Adventure</a></li>
              <li><a href="other-user-profile.html"><i class="icon-inbox"></i>Cartoon</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Comics</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Knowledge</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Magazine</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Music</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Novel</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Religion</a></li>
              <li><a href="other-user-listing.html"><i class="icon-inbox"></i>Science</a></li>
          </ul>
      </li>
        <li><a href="table.html"><i class="menu-icon icon-table"></i>-Coming Soon- </a></li>
        <li><a href="charts.html"><i class="menu-icon icon-bar-chart"></i>-Coming Soon- </a></li>
    </ul>

</div>
<!--/.sidebar-->
