﻿<!DOCTYPE html>
<html lang="en">
    @include('layouts.member.partials._head')

    <body>
        @include('layouts.member.partials._navbar')

        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        @include('layouts.member.partials._sidebar')
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            @yield('content')
                        </div>
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container" align="center">
                <b class="copyright">&copy; 2018 LaraLib </b>- All rights reserved.
            </div>
        </div>

        @include('layouts.member.partials._scripts')
    </body>
</html>
