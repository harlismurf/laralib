<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
	protected $fillable = [
        'title',
        'isbn',
        'edition',
        'volume',
        'issue',
        'publisher_id',
        'cover',
    ];

    public function publisher()
    {
    	// return $this->belongsTo('publisher');

    	return $this->belongsTo(Publisher::class, 'publisher_id', 'id');
    }
}
