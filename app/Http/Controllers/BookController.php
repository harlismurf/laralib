<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\book;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('librarian');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = DB::table('books')
                    ->leftJoin('publishers', 'books.publisher_id', '=', 'publishers.id')
                    ->get();

        // $publishers = DB::table('books')
        //             ->rightJoin('publishers', 'books.publisher_id', '=', 'publishers.id')
        //             ->get();
 
        $publishers = DB::table('publishers')->get(); 
                          
        return view('pages.librarian.books', compact('books', 'publishers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Book::create($request->all());

        return redirect()->route('books.index')
            ->withStatus('Successfully created a new book.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, book $book)
    {
        $book = Book::findOrFail($request->book_id);

        $book->update($request->all());

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $book = Book::findOrFail($request->book_id);

        $book->delete();
    }
}
